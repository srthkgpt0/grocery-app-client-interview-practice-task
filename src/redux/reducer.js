const initState = [];
export default function basketReducer(state = initState, action) {
  console.log(action.payload);
  switch (action.type) {
    case 'ADD_ITEM': {
      const item = state.find((i) => i.id === action.payload.id);
      const items = state.filter((i) => i.id !== action.payload.id);
      if (item) {
        return [
          ...items,
          {
            ...item,
            count: item.count + 1,
          },
        ];
      } else {
        return [
          ...state,
          {
            ...action.payload,
            count: 1,
          },
        ];
      }
    }

    case 'REMOVE_ITEM':
      const item = state.find((item) => item.id === action.payload);
      const items = state.filter((item) => item.id !== action.payload);
      if (item.count > 1) {
        return [
          ...items,
          {
            ...item,
            count: item.count - 1,
          },
        ];
      } else {
        return [...items];
      }

    default:
      return [...state];
  }
}
