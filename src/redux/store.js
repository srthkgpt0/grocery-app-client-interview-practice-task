import { combineReducers, createStore } from "redux";
import  basketReducer  from "./reducer";
const rootReducer = combineReducers({
    items:basketReducer
})
const store = createStore(rootReducer)

export default store