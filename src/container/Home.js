import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './styles.css';
function Home() {
  const dispatch = useDispatch();
  const [grocery] = useState([
    { id: 1, name: 'strawberry' },
    { id: 2, name: 'blueberry' },
    {
      id: 3,
      name: 'orange',
    },
    {
      id: 4,
      name: 'banana',
    },
    { id: 5, name: 'apple' },
    { id: 6, name: 'carrot' },
    {
      id: 7,
      name: 'celery',
    },
    { id: 8, name: 'mushroom' },
    { id: 9, name: 'green pepper' },
    { id: 10, name: 'eggs' },
    { id: 11, name: 'cheese' },
    { id: 12, name: 'butter' },
    { id: 13, name: 'chicken' },
    { id: 14, name: 'beef' },
    { id: 15, name: 'pork' },
    { id: 16, name: 'fish' },
    { id: 17, name: 'rice' },
    { id: 18, name: 'pasta' },
    { id: 19, name: 'bread' },
  ]);

  const handleAdd = (item) => {
    dispatch({ type: 'ADD_ITEM', payload: item });
  };
  const handleRemove = (id) => {
    dispatch({ type: 'REMOVE_ITEM', payload: id });
  };
  const items = useSelector((state) => state.items);

  return (
    <div>
      <h2>Basket App</h2>
      <div className='container'>
        <div>
          <h4>Grocery List</h4>
          <ul>
            {grocery.length > 0 &&
              grocery.map((item, index) => (
                <div className='listItem'>
                  <li key={item.id}>{item.name}</li>
                  <button onClick={() => handleAdd(item)}>Add</button>
                </div>
              ))}
          </ul>
        </div>
        <div>
          <h4>Basket Item</h4>
          <ul>
            {items.length > 0 &&
              items.map((item, index) => (
                <div className='listItem'>
                  <span className='counter'>
                    <span>{item.count}</span>
                    <li key={item.id}>{item.name}</li>
                  </span>
                  <button onClick={() => handleRemove(item.id)}>Remove</button>
                </div>
              ))}
          </ul>
        </div>
      </div>
      <footer className='footer'>Total Items: {items.length}</footer>
    </div>
  );
}

export default Home;
